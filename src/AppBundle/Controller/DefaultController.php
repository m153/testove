<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use LiqPay;
use AppBundle\Controller\CommentController;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="post_index")
     */
    public function indexAction(Request $request)
    {

        if (!empty($request->get('page'))) {
            $page = $request->get('page');
        }
        else{
            $page = 1;
        }
        

        $em = $this->getDoctrine()->getManager();
        $posts = $em->getRepository('AppBundle:Post')->findPostList($page);
        $count = ceil($em->getRepository('AppBundle:Post')->getCount()/10);  ;
     


        return $this->render('post/index.html.twig', array(
            'posts' => $posts,
            'page'  => $page,
            'count' => $count 
        ));
    }

    /**
     * Creates a new post entity.
     *
     * @Route("/new", name="post_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $public_key = $this->getParameter('public_key');
        $private_key = $this->getParameter('private_key');

        $sign = base64_encode( sha1($private_key .$request->get('data') . $private_key, 1 ));

        if (!$sign = $request->get('signature'))
        
            {
                $liqpay = new LiqPay($public_key, $private_key);
                $button = $liqpay->cnb_form(array(
                    'action'         => 'pay',
                    'amount'         => '10',
                    'currency'       => 'UAH',
                    'description'    => 'Оплата поста',
                    'language'       => 'uk',
                    'result_url'     => 'http://test2.morningrain.in.ua/new',
                    'version'        => '3',
                    'sandbox'        => '1',
                    'server_url'     => 'http://test2.morningrain.in.ua/new'
                    ));

               return $this->render('post/pay.html.twig', array('button' => $button
                ));
            }  
        else
            {
                $post = new Post();
                $form = $this->createForm('AppBundle\Form\PostType', $post);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $em = $this->getDoctrine()->getManager();
                    
                    $em->persist($post);
                    $em->flush($post);            

                    return $this->redirectToRoute('post_show', array('id' => $post->getId()));
        }

        return $this->render('post/new.html.twig', array(
            'post' => $post,
            'form' => $form->createView(),
        ));
            }
    }

    /**
     * Finds and displays a post entity.
     *
     * @Route("/{id}", name="post_show")
     * @Method({"GET", "POST"})
     */
    public function showAction(Post $post)
    {
        $em = $this->getDoctrine()->getManager();
        $comments = $em->getRepository('AppBundle:Comment')->findBy(
            array('postId' => $post),
            array('id' => 'DESC'));    



        return $this->render('post/show.html.twig', array(
            'post' => $post,
            'comments' =>  $comments));
    }

    /**
     *
     * @Route("/copy/post")
     * @Method({"GET", "POST"})
     */
    public function newsAction(Request $request)
    {
       $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository('AppBundle:Post')->findBy(
            array('id' => '27'),
            array());  

for ($i=0; $i < 60; $i++) { 
        $copy = new Post();

        $copy->setTitle($post[0]->getTitle());
        $copy->setPhoto($post[0]->getPhoto());
        $copy->setContent($post[0]->getContent());

                   $em->persist($copy);
                    $em->flush($copy);   

                    }


        return $this->redirectToRoute('post_show', array(
                        'id' =>  $copy->getId())


                        );
    }
    
   
}
